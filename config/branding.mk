# Copyright (C) 2018-23 The OolongOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#Oolong OS Versioning :
OOLONG_MOD_VERSION = Fourteen

ifndef OOLONG_BUILD_TYPE
    OOLONG_BUILD_TYPE := COMMUNITY
endif

# Test Build Tag
ifeq ($(OOLONG_TEST),true)
    OOLONG_BUILD_TYPE := DEVELOPER
endif

CURRENT_DEVICE=$(shell echo "$(TARGET_PRODUCT)" | cut -d'_' -f 2,3)
OOLONG_BUILD_DATE_UTC := $(shell date -u '+%Y%m%d-%H%M')
BUILD_DATE_TIME := $(shell date -u '+%Y%m%d%H%M')

ifeq ($(OOLONG_OFFICIAL), true)
   LIST = $(shell cat vendor/oolong/oolong.devices)
    ifeq ($(filter $(CURRENT_DEVICE), $(LIST)), $(CURRENT_DEVICE))
      IS_OFFICIAL=true
      OOLONG_BUILD_TYPE := RELEASE

#include vendor/oolong-priv/keys.mk
PRODUCT_PACKAGES += \
    Updater

    endif
    ifneq ($(IS_OFFICIAL), true)
       OOLONG_BUILD_TYPE := COMMUNITY
       $(error Device is not official "$(CURRENT_DEVICE)")
    endif
endif

ifeq ($(BUILD_WITH_GAPPS),true)
OOLONG_EDITION := GAPPS
else
OOLONG_EDITION := Vanilla
endif

ifeq ($(OOLONG_EDITION), GAPPS)
OOLONG_VERSION := OolongOS-$(OOLONG_MOD_VERSION)-$(CURRENT_DEVICE)-$(OOLONG_EDITION)-$(OOLONG_BUILD_TYPE)-$(OOLONG_BUILD_DATE_UTC)
OOLONG_FINGERPRINT := OolongOS/$(OOLONG_MOD_VERSION)/$(PLATFORM_VERSION)/$(OOLONG_BUILD_DATE_UTC)
OOLONG_DISPLAY_VERSION := OolongOS-$(OOLONG_MOD_VERSION)-$(OOLONG_BUILD_TYPE)
else
OOLONG_VERSION := OolongOS-$(OOLONG_MOD_VERSION)-$(CURRENT_DEVICE)-$(OOLONG_BUILD_TYPE)-$(OOLONG_BUILD_DATE_UTC)
OOLONG_FINGERPRINT := OolongOS/$(OOLONG_MOD_VERSION)/$(PLATFORM_VERSION)/$(OOLONG_BUILD_DATE_UTC)
OOLONG_DISPLAY_VERSION := OolongOS-$(OOLONG_MOD_VERSION)-$(OOLONG_BUILD_TYPE)
endif

TARGET_PRODUCT_SHORT := $(subst oolong_,,$(OOLONG_BUILD))

PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
  ro.oolong.version=$(OOLONG_VERSION) \
  ro.oolong.releasetype=$(OOLONG_BUILD_TYPE) \
  ro.modversion=$(OOLONG_MOD_VERSION) \
  ro.oolong.display.version=$(OOLONG_DISPLAY_VERSION) \
  ro.oolong.fingerprint=$(OOLONG_FINGERPRINT) \
  ro.build.datetime=$(BUILD_DATE_TIME) \
  ro.oolong.edition=$(OOLONG_EDITION)
